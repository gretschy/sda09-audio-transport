/*
  ==============================================================================

    FilePlayerGui.cpp
    Created: 22 Jan 2013 2:49:07pm
    Author:  tj3-mitchell

  ==============================================================================
*/

#include "FilePlayerGui.h"

FilePlayerGui::FilePlayerGui (FilePlayer& filePlayer_) : filePlayer (filePlayer_)
{
    playButton.setButtonText (">");
    playButton.addListener(this);
    addAndMakeVisible(&playButton);
    
    playbackSlider.setRange(0.0, 1.0);
    addAndMakeVisible(playbackSlider);
    playbackSlider.addListener(this);
    
    pitchSlider.setRange(0.01, 5.0);
    pitchSlider.setValue(1.0);
    addAndMakeVisible(pitchSlider);
    pitchSlider.addListener(this);
    
    gainSlider.setRange(0.0, 1.0);
    gainSlider.setValue(0.1);
    addAndMakeVisible(gainSlider);
    gainSlider.addListener(this);
    
    AudioFormatManager formatManager;
    formatManager.registerBasicFormats();
    fileChooser = new FilenameComponent ("audiofile",
                                         File::nonexistent,
                                         true, false, false,
                                         formatManager.getWildcardForAllFormats(),
                                         String::empty,
                                         "(choose a WAV or AIFF file)");
    fileChooser->addListener(this);
    addAndMakeVisible(fileChooser);
    
}

FilePlayerGui::~FilePlayerGui()
{
    delete fileChooser;
}


//Component
void FilePlayerGui::resized()
{
    playButton.setBounds (0, 0, getHeight(), getHeight()/4);
    playbackSlider.setBounds(0, getHeight()/4, getWidth(), getHeight()/4);
    pitchSlider.setBounds(0, (getHeight()/4) * 2, getWidth(), getHeight()/4);
    gainSlider.setBounds(0, (getHeight()/4) * 3, getWidth(), getHeight()/4);
    fileChooser->setBounds (getHeight(), 0, getWidth()-getHeight(), getHeight()/4);
}

//Button Listener
void FilePlayerGui::buttonClicked (Button* button)
{
    if (button == &playButton)
    {
        filePlayer.setPlaying(!filePlayer.isPlaying());
        if (filePlayer.isPlaying() == true)
        {
            startTimer(250);
        }

        else if (filePlayer.isPlaying() == false)
        {
            stopTimer();
        }
    }
}


//slider position
void FilePlayerGui::sliderValueChanged(Slider* slider)
{
    filePlayer.setPosition(playbackSlider.getValue());
    filePlayer.setPlaybackRate(pitchSlider.getValue());
    filePlayer.setGain(gainSlider.getValue());
}

//timer
void FilePlayerGui::timerCallback()
{
    playbackSlider.setValue(filePlayer.getPosition());
}

//FilenameComponentListener
void FilePlayerGui::filenameComponentChanged (FilenameComponent* fileComponentThatHasChanged)
{
    if (fileComponentThatHasChanged == fileChooser)
    {
        File audioFile (fileChooser->getCurrentFile().getFullPathName());
        
        if(audioFile.existsAsFile())
        {
                filePlayer.loadFile(audioFile);
        }
        else
        {
            AlertWindow::showMessageBox (AlertWindow::WarningIcon,
                                         "sdaTransport",
                                         "Couldn't open file!\n\n");
        }
    }
}