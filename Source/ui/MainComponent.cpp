/*
  ==============================================================================

    This file was auto-generated!

  ==============================================================================
*/

#include "MainComponent.h"


//==============================================================================
MainComponent::MainComponent (Audio& audio_) : audio (audio_)
{
    for (int i=0; i < numFilePlayerGuis; i++)
    {
        filePlayerGuis.add (new FilePlayerGui(audio.getFilePlayer(i)));
        addAndMakeVisible (filePlayerGuis[i]);
    }
    setSize (500, 400);
}

MainComponent::~MainComponent()
{
}

void MainComponent::resized()
{
    const int width = getWidth();
    const int height = 80;
    
    for(int i=0; i < numFilePlayerGuis; i++)
    {
        filePlayerGuis[i]->setBounds(0, i * height, width, height);
    }
    
}

//MenuBarCallbacks==============================================================
StringArray MainComponent::getMenuBarNames()
{
    const char* const names[] = { "File", 0 };
    return StringArray (names);
}

PopupMenu MainComponent::getMenuForIndex (int topLevelMenuIndex, const String& menuName)
{
    PopupMenu menu;
    if (topLevelMenuIndex == 0)
        menu.addItem(AudioPrefs, "Audio Preferences", true, false);
    return menu;
}

void MainComponent::menuItemSelected (int menuItemID, int topLevelMenuIndex)
{
    if (topLevelMenuIndex == FileMenu)
    {
        if (menuItemID == AudioPrefs)
        {
            AudioDeviceSelectorComponent audioSettingsComp (audio.getAudioDeviceManager(),
                                                            0, 2, 2, 2, true, true, true, false);
            audioSettingsComp.setSize (450, 350);
            DialogWindow::showModalDialog ("Audio Settings",
                                           &audioSettingsComp, this, Colours::lightgrey, true);
        }
    }
}

